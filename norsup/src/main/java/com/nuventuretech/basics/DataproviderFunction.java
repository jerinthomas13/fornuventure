package com.nuventuretech.basics;

import org.testng.annotations.DataProvider;

import com.nuventure.utils.DataProviderInput;



public class DataproviderFunction extends Functions {
	
protected String dataSheetName;
	
	@DataProvider(name="fetchData")
	public Object[][] getData(){
		return DataProviderInput.getSheet(dataSheetName);		
	}


}

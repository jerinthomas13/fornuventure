package com.nuventuretech.basics;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;




public class Functions {
	
	protected WebDriver driver;
	protected static Properties prop;
	String browser ;
	String sURL;
	
	public void invokeApp(String browser) {
		this.browser=browser;
		
		prop = new Properties();
		try {
			prop.load(new FileInputStream(new File("./Config/Config.properties")));
			sURL = prop.getProperty("URL");
			System.out.println("The URL is "+sURL);
			String username = prop.getProperty("username");
			String password = prop.getProperty("password");
			System.out.println(username+""+password);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	if(browser.equalsIgnoreCase("chrome")){
		
		System.setProperty("webdriver.chrome.driver", "./driverss/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(sURL);
		

	}else if(browser.equalsIgnoreCase("firefox")){
		System.setProperty("webdriver.gecko.driver", "./driverss/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(sURL);
		
	}		}

	
	
	    
		
	}



package com.nuventure.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.nuventuretech.basics.Functions;



public class AddPoolPage extends Functions {
	
	public AddPoolPage (WebDriver driver) 
	{
		this.driver = driver;
		
	}
	
public AddPoolPage enterUserName()
{
	driver.findElement(By.xpath("//input[@name='username']"));
	return this;
}

public AddPoolPage enterPassWord()
{
	driver.findElement(By.xpath("//input[@name='password']"));
	return this;
}

public AddPoolPage clickOnSignIn()
{
	driver.findElement(By.xpath("//button[@type='submit']"));
	return this;
}
}

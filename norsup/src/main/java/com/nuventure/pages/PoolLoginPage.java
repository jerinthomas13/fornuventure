 package com.nuventure.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.nuventuretech.basics.Functions;

public class PoolLoginPage extends Functions {
	
	public PoolLoginPage (WebDriver driver) 
	{
		this.driver = driver;
		
	}
	
public PoolLoginPage enterUserName(String username)
{
	driver.findElement(By.xpath("//input[@name='username']")).sendKeys(username);
	return this;
}

public PoolLoginPage enterPassWord(String password)
{
	driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
	return this;
}

public PoolLoginPage clickOnSignIn()
{
	driver.findElement(By.xpath("//button[@type='submit']")).click();
	return this;
}

public PoolLoginPage getTitleOfPage()
{
	String title = driver.getTitle();
	System.out.println("The title of dashboard"+title);
	
	return this;
}
}
package com.nuventure.testcases;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.nuventure.pages.PoolLoginPage;
import com.nuventuretech.basics.DataproviderFunction;

public class TC_Login extends DataproviderFunction {
	
	

	@BeforeClass
	public void values(){
		dataSheetName = "Credentials";
	}
	
	@Test(dataProvider = "fetchData")
	public void LoginApp(String username,String password)
	{
		invokeApp("chrome");
		new PoolLoginPage(driver)
		.enterUserName(username)
		.enterPassWord(password)
		.clickOnSignIn()
		.getTitleOfPage();
	
	
	}
	

}
